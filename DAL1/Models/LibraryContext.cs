﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace Task23_Advanced.Models
{
    public class Library1Context : DbContext
    {
        public Library1Context() : base("Library1Context")
        {
        }
        /// <summary>
        /// <c>GetNews()</c> method to get list of news from database
        /// </summary>
        /// <returns><c>List<News></c></returns>
        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                string errorMessages = string.Join("; ", ex.EntityValidationErrors.SelectMany(x => x.ValidationErrors).Select(x => x.PropertyName + ": " + x.ErrorMessage));
                throw new DbEntityValidationException(errorMessages);
            }
        }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Questionnaire> Questionnaires { get; set; }
        public DbSet<Article> Articles { get; set; }
    }
}