﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Advanced.Models;

namespace DAL1.Models
{
    public class NewsRepository : IRepository<News>
    {
        private readonly Library1Context _db;
        public NewsRepository(Library1Context context)
        {
            this._db = context;
        }
        public IEnumerable<News> GetAll()
        {
            return _db.News.ToList();
        }
    }
}
