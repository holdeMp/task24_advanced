﻿using DAL1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task23_Advanced.Models;

namespace Task23_Advanced.Controllers
{
    public class HomeController : Controller
    {
        // GET: Main
        public ActionResult Index()
        {
            var context = new Library1Context();
            var db = new NewsRepository(context);
            return View(db.GetAll());
        }
       
    }
}