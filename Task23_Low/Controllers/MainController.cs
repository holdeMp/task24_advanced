﻿using DAL1.Models;
using System.Web.Mvc;
using Task23_Advanced.Models;

namespace Task23_Advanced.Controllers
{
    public class MainController : Controller
    {
        // GET: Main
        public ActionResult Index()
        {
            var context = new Library1Context();
            var db = new ArticleRepository(context);
            return View(db.GetAll());
        }
    }
}