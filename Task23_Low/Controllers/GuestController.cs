﻿using DAL1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task23_Advanced.Models;

namespace Task23_Advanced.Controllers
{
    public class GuestController : Controller
    {
        // GET: Guest
        public ActionResult Index()
        {
            var context = new Library1Context();
            var db = new ReviewsRepository(context);
            return View(db.GetAll());
        }
    }
}